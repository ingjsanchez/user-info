<?php

namespace Ingjsanchez\UserInfo\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;

class UserInfoController extends Controller
{
    public function index()
    {
        $users = User::all();

        if (!$users->first()) {
            return response()->json([
                "message" => "No se encontraron usuarios.",
            ]);
        }

        return response()->json([
            "users" => $users,
        ]);
    }
}
